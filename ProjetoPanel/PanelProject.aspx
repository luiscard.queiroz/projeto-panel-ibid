﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PanelProject.aspx.cs" Inherits="ProjetoPanel.PanelProject" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="StyleSheet1.css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Projeto Painel</h1>
        <table class="box-item" style="width: 390px; height: 250px;">
            <%--<tr class="linhaTitulo">
                <td colspan="1" style="text-align: center;"><h1>Projeto Painel</h1></span></td>
            </tr>--%>
            
            <tr> <%--Linha da tabela com os 3 paineis--%>
                <td style="width: 400px; text-align:  center;" >

                    <asp:Panel ID="Panel1" runat="server" style="width: 100%;">
                        
                        <asp:Panel ID="Panel2DadosPessoais" runat="server"> <%--Iniciando painel 2--%> 
                            <table class="tableAll tableP2" style="width: 100%;">
                                <tr>
                                    <td colspan="2" style="text-align: center;"><strong><span>Dados pessoais</span></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Nome:</td>
                                    <td>
                                        <asp:TextBox required="true"  class="input" style="width: 100%" ID="txtNome" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Sobrenome:</td>
                                    <td>
                                        <asp:TextBox required="true"  class="input" style="width: 100%" ID="txtSobrenome" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Gênero:</td>
                                    <td>
                                        <asp:RadioButton ID="rdbMasculino" runat="server" Text="Masculino" GroupName="genero" />
                                        <asp:RadioButton ID="rdbFeminino" runat="server" Text="Feminino" GroupName="genero" Checked="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Telefone:</td>
                                    <td>
                                        <asp:TextBox required="true"  class="input" placeholder="(XX) XXXXX - XXXX" ID="txtNumero" runat="server" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <asp:Button CssClass="btnAll" ID="btnUm" runat="server" Text="Próximo" OnClick="btnUm_Click" /></td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <asp:Panel ID="Panel3Endereco" runat="server"> <%--Iniciando painel 3--%>
                            <table class="tableAll tableP3" style="width: 100%;">
                                <tr>
                                    <td colspan="2" style="text-align: center;"><strong><span>Endereço</span></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Endereço:</td>
                                    <td>
                                        <asp:TextBox  class="input" style="width: 100%;" ID="txtEndereco" runat="server"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Cidade:</td>
                                    <td>
                                        <asp:TextBox  class="input" style="width: 100%;" ID="txtCidade" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >CEP</td>
                                    <td>
                                        <asp:TextBox  class="input" style="width: 100%;" ID="txtCep" runat="server" TextMode="Number"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <asp:Button CssClass="btnAll" ID="btnDois" runat="server" Text="Voltar" OnClick="btnDois_Click" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <asp:Button CssClass="btnAll" ID="btnTres" runat="server" Text="Próximo" OnClick="btnTres_Click" /></td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <asp:Panel ID="Panel4Login" runat="server"> <%--Iniciando painel 4--%>
                            <table class="tableAll tableP4" style="width: 100%;">
                                <tr>
                                    <td colspan="2" style="text-align: center;"><strong><span>Login</span></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Usuário:</td>
                                    <td>
                                        <asp:TextBox class="input" style="width: 100%;" ID="txtUsuario" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: right; width: 100px;" >Senha:</td>
                                    <td>
                                        <asp:TextBox  class="input" style="width: 100%;" ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <asp:Button CssClass="btnAll" ID="btnQuatro" runat="server" Text="Voltar" OnClick="btnQuatro_Click" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <asp:Button CssClass="btnAll" ID="btnCinco" runat="server" Text="Enviar" OnClick="btnCinco_Click" /></td>
                                </tr>
                            </table>
                        </asp:Panel>

                    </asp:Panel>

                </td>    
            </tr>

            <tr>
                <td colspan="3" style="text-align: center;">
                    <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
