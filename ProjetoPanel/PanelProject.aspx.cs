﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjetoPanel
{
    public partial class PanelProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Panel2DadosPessoais.Visible = true;
                Panel3Endereco.Visible = false;
                Panel4Login.Visible = false;
            }
        }

        protected void btnUm_Click(object sender, EventArgs e)
        {
            Panel2DadosPessoais.Visible = false;
            Panel3Endereco.Visible = true;
            Panel4Login.Visible = false;
        }

        protected void btnDois_Click(object sender, EventArgs e)
        {
            Panel2DadosPessoais.Visible = true;
            Panel3Endereco.Visible = false;
            Panel4Login.Visible = false;
        }

        protected void btnTres_Click(object sender, EventArgs e)
        {
            if(txtEndereco.Text == "" || txtCidade.Text == "" || txtCep.Text == "")
            {
                lblResultado.Text = "Verifique se todos os campos estão preenchidos.";
            } else
            {
                Panel2DadosPessoais.Visible = false;
                Panel3Endereco.Visible = false;
                Panel4Login.Visible = true;
            }
        }

        protected void btnQuatro_Click(object sender, EventArgs e)
        {
            Panel2DadosPessoais.Visible = false;
            Panel3Endereco.Visible = true;
            Panel4Login.Visible = false;
        }

        protected void btnCinco_Click(object sender, EventArgs e)
        {
            string nome = txtNome.Text;
            if (txtUsuario.Text == "" || txtSenha.Text == "")
            {
                lblResultado.Text = "Verifique se todos os campos estão preenchidos.";
            } else
            {
                Response.Write("<script>alert('Dados enviados com sucesso!');</script>");
                txtCep.Text = "";
                txtCidade.Text = "";
                txtEndereco.Text = "";
                txtNome.Text = "";
                txtSobrenome.Text = "";
                txtNumero.Text = "";
                txtUsuario.Text = "";
                txtSenha.Text = "";
                lblResultado.Text = "";
            }
        }
    }
}